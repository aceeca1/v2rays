#ifndef SESSION_H
#define SESSION_H

#include <QWebSocket>

class Session : public QObject {
    Q_OBJECT
public:
    using QObject::QObject;
    void SetClient(QWebSocket* client) { client_ = client; }
    void ReceiveFromClient(const QByteArray& data);
    void ClientDisconnected() { return ServerDisconnected(); }
    
private:
    void ReceiveFromClient0(const QByteArray& data);
    void ReceiveFromClient1(const QByteArray& data);
    void ReceiveFromClient2(const QByteArray& data);

    void ReceiveFromServer(const QByteArray& data);

    void ConnectToIP4(QDataStream* stream);
    void ConnectToIP6(QDataStream* stream);
    void ConnectToDomain(QDataStream* stream);

    void Connected(bool success);
    void ServerConnected();
    void ServerDisconnected();
    void ServerReadyRead();
    
    int phase_ = 0;
    QWebSocket* client_;
    QTcpSocket* server_;
};

#endif