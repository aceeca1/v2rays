#include "session.h"
#include "server.h"

#include <QDataStream>

void Session::ReceiveFromClient(const QByteArray& data) {
    switch (phase_) {
        case 0:
            return ReceiveFromClient0(data);
        case 1:
            return ReceiveFromClient1(data);
        case 2:
            return ReceiveFromClient2(data);
    }
}

void Session::ReceiveFromClient0(const QByteArray&) {
    client_->sendBinaryMessage(QByteArray::fromRawData("\x05\x00", 2));
    phase_ = 1;
}

void Session::ReceiveFromClient1(const QByteArray& data) {
    server_ = new QTcpSocket(this);
    connect(server_, &QTcpSocket::connected,  //
            this, &Session::ServerConnected);
    connect(server_, &QTcpSocket::disconnected,  //
            this, &Session::ServerDisconnected);
    connect(server_, &QTcpSocket::readyRead,  //
            this, &Session::ServerReadyRead);
    QDataStream stream(data);
    stream.skipRawData(3);
    quint8 c;
    stream >> c;
    if (c == 1) return ConnectToIP4(&stream);
    if (c == 3) return ConnectToDomain(&stream);
    if (c == 4) return ConnectToIP6(&stream);
}

void Session::ReceiveFromClient2(const QByteArray& data) {
    server_->write(data);
}

void Session::ReceiveFromServer(const QByteArray& data) {
    client_->sendBinaryMessage(data);
}

void Session::ConnectToIP4(QDataStream* stream) {
    quint32 ip4Addr;
    quint16 port;
    *stream >> ip4Addr >> port;
    server_->connectToHost(QHostAddress(ip4Addr), port);
}

void Session::ConnectToIP6(QDataStream* stream) {
    quint8 ip6Addr[16];
    quint16 port;
    stream->readRawData((char*)ip6Addr, 16);
    *stream >> port;
    server_->connectToHost(QHostAddress(ip6Addr), port);
}

void Session::ConnectToDomain(QDataStream* stream) {
    quint8 hostNameSize;
    QByteArray hostName;
    quint16 port;
    *stream >> hostNameSize;
    hostName.resize(hostNameSize);
    stream->readRawData(hostName.data(), hostNameSize);
    *stream >> port;
    server_->connectToHost(hostName, port);
}

void Session::Connected(bool success) {
    client_->sendBinaryMessage(QByteArray::fromRawData(
        success ? "\x05\x00\x00\x01\x00\x00\x00\x00\x00\x00"
                : "\x05\x01\x00\x01\x00\x00\x00\x00\x00\x00",
        10));
    phase_ = success ? 2 : 0;
}

void Session::ServerConnected() {
    if (phase_ == 1) return Connected(true);
}

void Session::ServerDisconnected() {
    if (phase_ == 1) Connected(false);
    auto server = (Server*)parent();
    server->RemoveSession(client_);
    deleteLater();
}

void Session::ServerReadyRead() {
    return ReceiveFromServer(server_->read(server_->bytesAvailable()));
}
