#ifndef SERVER_H
#define SERVER_H

#include "session.h"

#include <QWebSocket>
#include <QWebSocketServer>

class Server : public QObject {
    Q_OBJECT
public:
    Server() : server_("", QWebSocketServer::NonSecureMode) {}
    bool Listen(int port);
    void RemoveSession(QWebSocket* socket) { sessions_.remove(socket); }

private:
    void OnNewConnection();
    void OnReceive(const QByteArray& data);

    QWebSocketServer server_;
    QHash<QWebSocket*, Session*> sessions_;
};

#endif
