FROM trusty
RUN add-apt-repository ppa:beineri/opt-qt-5.10.1-trusty
RUN apt-get update
RUN apt-get -y install qt510base qt510websockets
RUN qmake
RUN make
