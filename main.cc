#include "server.h"

#include <QCoreApplication>

int main(int argc, char** argv) {
    QCoreApplication _(argc, argv);
    
    Server server;
    server.Listen(8080);

    QCoreApplication::exec();
    return 0;
}
