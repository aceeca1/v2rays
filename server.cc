#include "server.h"

bool Server::Listen(int port) {
    if (!server_.listen(QHostAddress::Any, port)) return false;
    connect(&server_, &QWebSocketServer::newConnection,  //
            this, &Server::OnNewConnection);
    return true;
}

void Server::OnNewConnection() {
    auto socket = server_.nextPendingConnection();
    auto session = sessions_[socket] = new Session(this);
    connect(socket, &QWebSocket::binaryMessageReceived,  //
            this, &Server::OnReceive);
    connect(socket, &QWebSocket::disconnected,  //
            session, &Session::ClientDisconnected);
}

void Server::OnReceive(const QByteArray& data) {
    auto socket = (QWebSocket*)sender();
    auto& session = sessions_[socket];
    session->SetClient(socket);
    session->ReceiveFromClient(data);
}
